const browserSync = require("browser-sync");
const BASE_URL = "https://hopialikeit.myshopify.com";
const PREVIEW_QUERY = "?preview_theme_id=80496984111";

browserSync({
  proxy: `${BASE_URL}${PREVIEW_QUERY}`,
  //files: "browserUpdate.js"
  files: ["src/styles/index.css", "src/vue/**/*.vue", "src/vue/**/*.js,", "src/layout/*.liquid", "src/templates/*.liquid"],
  reloadDelay: 1000,
  snippetOptions: {
    rule: {
        match: /<\/body>/i,
        fn: function (snippet, match) {
            return snippet + match;
        }
    }
}
});
